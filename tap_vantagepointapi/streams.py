"""Stream type classes for tap-vantagepointapi."""

from __future__ import annotations

import typing as t
from pathlib import Path

import requests
from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_vantagepointapi.client import vantagepointAPIStream

class EmployeeStream(vantagepointAPIStream):
    name = "employees"
    path = "/employee"
    replication_key = None

class ProjectStream(vantagepointAPIStream):
    name = "projects"
    path = "/project"
    replication_key = None

    def get_child_context(self, record: dict, context: dict | None) -> dict | None:
        key = record["key"]
        special_characters = {'&', '<', '>', '"', "'", '/', '\\', '%', '=', '?', '#', '+'}
        if not any(char in special_characters for char in key):
            return {
                "key": key,
            }
        else:
            return None
    
    def _sync_children(self, child_context: dict) -> None:
        if child_context:
            return super()._sync_children(child_context)

class ProjectTeamMemberStream(vantagepointAPIStream):
    name = "Project Team Members"
    path = "/project/{key}/teammember"
    parent_stream_type = ProjectStream
    ignore_parent_replication_key = True
    replication_key = None
    state_partitioning_keys = ["key", "RecordID"]
    schema = th.PropertiesList(
        th.Property("WBS1", th.StringType),
        th.Property("WBS2", th.StringType),
        th.Property("WBS3", th.StringType),
        th.Property("RecordID", th.StringType),
        th.Property("PrimaryInd", th.StringType),
        th.Property("Role", th.StringType),
        th.Property("RoleDescription", th.StringType),
        th.Property("CFRoleDescription", th.StringType),
        th.Property("lfName", th.StringType),
        th.Property("flName", th.StringType),
        th.Property("ctName", th.StringType),
        th.Property("FirmName", th.StringType),
        th.Property("FirmID", th.StringType),
        th.Property("Title", th.StringType),
        th.Property("Phone", th.StringType),
        th.Property("Address1", th.StringType),
        th.Property("City", th.StringType),
        th.Property("State", th.StringType),
        th.Property("Zip", th.StringType),
        th.Property("EMail", th.StringType),
        th.Property("RecordType", th.StringType),
        th.Property("StartDate", th.DateTimeType),
        th.Property("EndDate", th.DateTimeType),
        th.Property("Hours", th.IntegerType),
        th.Property("ModUser", th.StringType),
        th.Property("CreateUser", th.StringType),
        th.Property("ModDate", th.DateTimeType),
        th.Property("CreateDate", th.DateTimeType),
        th.Property("Employee", th.StringType, description="The user's ID number"),
        th.Property("AjeraSync", th.StringType),
    ).to_dict()