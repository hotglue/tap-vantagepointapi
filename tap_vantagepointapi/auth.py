"""vantagepointAPI Authentication."""

from __future__ import annotations

from singer_sdk.authenticators import OAuthAuthenticator, SingletonMeta


# The SingletonMeta metaclass makes your streams reuse the same authenticator instance.
# If this behaviour interferes with your use-case, you can remove the metaclass.
class vantagepointAPIAuthenticator(OAuthAuthenticator, metaclass=SingletonMeta):
    """Authenticator class for vantagepointAPI."""

    @property
    def oauth_request_body(self) -> dict:
        """Define the OAuth request body for the AutomaticTestTap API.

        Returns:
            A dict with the request body
        """
        return {
            "username": self.config["username"],
            "password": self.config["password"],
            "grant_type": "password",
            "Integrated": "N",
            "database": self.config["database"],
            "client_id": self.config["client_id"],
            "client_secret": self.config["client_secret"]
        }

    @classmethod
    def create_for_stream(cls, stream, url) -> vantagepointAPIAuthenticator:  # noqa: ANN001
        """Instantiate an authenticator for a specific Singer stream.

        Args:
            stream: The Singer stream instance.

        Returns:
            A new authenticator.
        """
        return cls(
            stream=stream,
            auth_endpoint=url + "/token",
            oauth_scopes="openid"
        )
