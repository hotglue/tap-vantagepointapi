- # tap-vantagepointapi

`tap-vantagepointapi` is a Singer tap for vantagepointAPI.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.


```bash
pipx install tap-vantagepointapi
```

Install from GitHub:

```bash
pipx install git+https://gitlab.com/chauncywilson/tap-vantagepointapi@main
```


## Installation

tap_vantagepointapi uses poetry to manage its virtual environments. Below are some instructions to properly
install tap_vantagepointapi onto your system

if you do not have poetry installed on your machine use these commands in the command terminal:
(If you have pipx installed on the machine, that step may be skipped)

```bash
pip install pipx
```
```bash
pipx install poetry
```


## Configuration

You will need to provide a configuration file and example will be shown in the config.json.template file

Replace the values with your own data types provided by Deltek Vantagepoint

The data in these files is sensitive, we suggest putting them in the .secrets folder

The required data types are
-   Client_ID
-   Client_Secret
-   Database


### Accepted Config Options

A full list of supported settings and capabilities for this
tap is available by running:

```bash
tap-vantagepointapi --about
```

Additional Config options include:
-   Username and Password
-   Username and Refresh Token

One of these must be present to extract data

### Configure using environment variables

This Singer tap will automatically import any environment variables within the working directory's
`.env` if the `--config=ENV` is provided, such that config values will be considered if a matching
environment variable is set either in the terminal context or in the `.env` file.

A bash command to run the tap may look as follows.

```bash
file\to\path\tap-vantagepointapi> poetry run tap-vantagepointapi --config=.secrets\config.json > output\out.jsonl
```

### Source Authentication and Authorization

Deltek Vantagepoint uses OAuth 2.0

Client_ID: Provided and validated through Deltek Vantagepoint.
Client_Secret: Provided by Deltek Vantagepoint when a Client_ID is given.
Authorization: Username and Password
      

## Usage

You can easily run `tap-vantagepointapi` by itself or in a pipeline using [Meltano](https://meltano.com/).


### Executing the Tap Directly

```bash
tap-vantagepointapi --version
tap-vantagepointapi --help
tap-vantagepointapi --config CONFIG --discover > ./catalog.json
```

## Developer Resources

Follow these instructions to contribute to this project.

### Initialize your Development Environment

```bash
pipx install poetry
poetry install
```

### Create and Run Tests

Create tests within the `tests` subfolder and
  then run:

```bash
poetry run pytest
```

You can also test the `tap-vantagepointapi` CLI interface directly using `poetry run`:

```bash
poetry run tap-vantagepointapi --help
```

### Testing with [Meltano](https://www.meltano.com)

_**Note:** This tap will work in any Singer environment and does not require Meltano.
Examples here are for convenience and to streamline end-to-end orchestration scenarios._


Next, install Meltano (if you haven't already) and any needed plugins:

```bash
# Install meltano
pipx install meltano
# Initialize meltano within this directory
cd tap-vantagepointapi
meltano install
```

Now you can test and orchestrate using Meltano:

```bash
# Test invocation:
meltano invoke tap-vantagepointapi --version
# OR run a test `elt` pipeline:
meltano elt tap-vantagepointapi target-jsonl
```

### SDK Dev Guide

See the [dev guide](https://sdk.meltano.com/en/latest/dev_guide.html) for more instructions on how to use the SDK to
develop your own taps and targets.
