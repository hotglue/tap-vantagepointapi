"""vantagepointAPI tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_vantagepointapi import streams
streams.EmployeeStream
streams.ProjectStream
streams.ProjectTeamMemberStream


class TapvantagepointAPI(Tap):
    """vantagepointAPI tap class."""

    name = "tap-vantagepointapi"

    config_jsonschema = th.PropertiesList(
        th.Property("username", th.StringType),
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True, secret=True),
        th.Property("database", th.StringType, required=True),
        th.Property("password", th.StringType, secret=True),
        th.Property("refresh_token", th.StringType),
        th.Property("expires_in", th.IntegerType, description="Time in seconds until the token expires"),
        th.Property("start_date", th.DateTimeType, description="The start date to pull data from"),
        th.Property("access_token", th.StringType, description="The current access token must be a Bearer Oauth2 token"),
        th.Property("url", th.StringType, description="The url to the vantagepointAPI"),
        th.Property("pageSize", th.IntegerType, default=2500),
        ).to_dict()

    def discover_streams(self) -> list[streams.vantagepointAPIStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.EmployeeStream(self),
            streams.ProjectStream(self),
            streams.ProjectTeamMemberStream(self),
        ]


if __name__ == "__main__":
    TapvantagepointAPI.cli()