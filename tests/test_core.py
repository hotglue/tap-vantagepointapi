"""Tests standard tap features using the built-in SDK tests library."""

import datetime

from singer_sdk.testing import get_tap_test_class

from tap_vantagepointapi.tap import TapvantagepointAPI

SAMPLE_CONFIG = {
    "start_date": datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d"),
    "client_id": "",
    "client_secret": "",
    "database": "",
    "url": "",
    "username": "",
    "password": ""
}


# Run standard built-in tap tests from the SDK:
TestTapvantagepointAPI = get_tap_test_class(
    tap_class=TapvantagepointAPI,
    config=SAMPLE_CONFIG,
)


# TODO: Create additional tests as appropriate for your tap.
